# eAcademy - Digital Education Platform - for the new normal

## Overview
Single Website cum App for various schools and teachers to upload their class room lectures, homework content and make it available for students to learn and engage
we deliver a simpler, more powerful teaching and learning experience that goes beyond the traditional learning management system

## Goals
Target Audience : Tier 2 city schools > Tier 1 schools

Same model for all schools (not much customization)

Roles in a school: Admin, Teacher, Student (+ SuperAdmin)

## Phases

## TEAM (With roles and responsibilities):
* Application Architect cum BE developer -- padodi
* Technical Lead cum FE developer -- kaka
* Project Manager cum Data Analyst cum Database Management -- multhu
* Business Marketing executive cum tester -- bhalu 
* Hosting, deployment & UI/UX -- ustarike-se

## Content
## Design

## Functionality

### Student 

* Account (add thumbnail)
* Notice Board/ Announcements(4)(youtube stories/ watsapp status like carousel)
* Submit Assignments
* View Exam wise Marksheets
* Live APIs( weather, time, corona update API)
* Get Fee notification and see fee structure (limited features for defaulting students)
* Test according 

### Teacher (needs to be assigned to a class)

* Populate Student List ( School > class > section > list)(tiles)  
* Add Marks
* Add Assignment/ Homework 
* Student Feedback
* Share files/ notes
* Attendance ( send mail/ response)
* Schedule Exam MCQ/ Open book(future add on) - Create Test


### Admin (Organisation head)

* Create Teacher, Students
* View number of teachers, students
* Generate Marksheets ( add on)
* Add Examination structure
* Marks defaulter students

## Accessibility
## Hosting
## Browser Support
## Milestones
## Deadlines

Digital India AatmaNirbhar Bharat Innovate Challenge - Submission ID: 5763523 - July 18 2020

## Budget
## Technology Stack: 

* Node.js
* Express.js
* react.js with bootstrap
* redux
* Yugabyte cloud

## Cloud Stack

* AWS Lambda
* AWS S3

## Marketing - Pitch according to school fee structure

## Business model -pricing according to number of students and features added on top of basic offering

* Standard -- < 1000 students (basic features)
* Premium  -- >1000 <5000 students (choice of add-ons available)
* Ultimate -- >5000 students (fully loaded)