import React from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';

export default class PersonList extends React.Component {

  state = {
    persons: []
  }

  componentDidMount() {
    var config = {
      headers: {'Access-Control-Allow-Origin': '*'}
  };
    axios.get(`http://localhost:8080/pers/Siddharth`, config)
    .then(res => {
      const persons = res.data;
      console.log(res.data);
      this.setState({ persons });
    })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <ul>
            { this.state.persons.map(person => <li>{person.age}</li>)}
          </ul>
        </header>
      </div>
    );
  }


}